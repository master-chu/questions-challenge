import React from 'react';

export default class CreateButton extends React.PureComponent {
  render() {
    let { onClick } = this.props;
    return (
      <button 
        onClick={onClick} 
        type="button" 
        className="button is-primary is-small"> 
        Create New Question
      </button>
    );
  }
}