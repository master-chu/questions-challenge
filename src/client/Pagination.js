import React from 'react';
import Dropdown from './Dropdown';

export default class Pagination extends React.PureComponent {
  constructor(props) {
    super(props);

    this.firstPage = this.firstPage.bind(this);
    this.lastPage = this.lastPage.bind(this);
    this.makePageNumbers = this.makePageNumbers.bind(this); 
    this.selectPage = this.selectPage.bind(this); 
  }

  render() {
    let { 
      currentPage,
      onSelectPage 
    } = this.props;

    let pagesOptions = this.makePageNumbers().map((page) => {
      return { value: page };
    });

    return (
      <React.Fragment>
        <Dropdown 
          label="Page"
          options={pagesOptions}
          currentValue={currentPage}
          onChange={this.selectPage} />
        <nav className="pagination is-small" role="navigation" aria-label="pagination">
          <button 
            type="button"
            disabled={currentPage === this.firstPage()}
            onClick={() => this.selectPage(currentPage - 1)}
            className="pagination-previous">Previous</button>
          <button
            type="button"
            disabled={currentPage === this.lastPage()}
            onClick={() => this.selectPage(currentPage + 1)}
            className="pagination-next">Next page</button>
        </nav>
      </React.Fragment>
    );
  }

  makePageNumbers() {
    let pages = [];
    for (let i = this.firstPage(); i <= this.lastPage(); i++) {
      pages.push(i);
    }
    return pages;
  }

  firstPage() {
    return 1;
  }

  lastPage() {
    let {
      numRecords,
      pageSize
    } = this.props;

    return Math.floor(numRecords / pageSize);
  }

  selectPage(page = 1) {
    let { onSelectPage } = this.props;
    return onSelectPage(parseInt(page));
  }
}
