import React from 'react';

export default class Notification extends React.PureComponent {
  render() {
    let { 
      type,
      content,
      onClickDelete 
    } = this.props;

    if (content) {
      return (
        <div className={`notification is-${type}`}>
          <button onClick={onClickDelete} className="delete"></button>
          {content}
        </div>
      );
    } else {
      return null;
    }
  }
}