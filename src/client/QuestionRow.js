import React from 'react';
import DeleteButton from './DeleteButton';

export default class QuestionRow extends React.PureComponent {
  constructor(props) {
    super(props);

    this.promptForDeletion = this.promptForDeletion.bind(this);
  }

  render() {
    let {
      questionId,
      questionText,
      answer,
      distractors
    } = this.props;

    return (
      <tr key={questionId}>
        <td>
          {questionId}
        </td>
        <td>
          {questionText}
        </td>
        <td>
          {answer}
        </td>
        <td>
          {distractors}
        </td>
        <td>
          <DeleteButton 
            questionId={questionId}
            onClick={this.promptForDeletion} />
        </td>
      </tr>
    );
  }

  promptForDeletion() {
    let {
      questionId,
      questionText,
      deleteQuestion
    } = this.props;

    let shouldDelete = window.confirm(
      `Are you sure you want to delete question "${questionText}"?`
    );

    if (shouldDelete) {
      deleteQuestion(questionId, questionText);
    }
  }
}