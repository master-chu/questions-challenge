import React from 'react';

export default class DeleteButton extends React.PureComponent {
  render() {
    let { onClick } = this.props;
    return (
      <button 
        onClick={onClick} 
        type="button" 
        className="button is-danger is-outlined is-small"> 
        Delete
      </button>
    );
  }
}