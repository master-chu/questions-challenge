import React from 'react';

export default class Dropdown extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.makeOptionElement = this.makeOptionElement.bind(this);
  }

  render() {
    let { 
      label, 
      options,
      currentValue
    } = this.props;

    let optionElements = options.map(this.makeOptionElement);

    return (
      <React.Fragment>
        <strong> {`${label }`} </strong>
        <div className="select is-small">
          <select defaultValue={currentValue} onChange={this.handleChange}>
            {optionElements}
          </select>
        </div>
      </React.Fragment>
    );
  }

  handleChange(event) {
    let { onChange } = this.props;
    let selectedValue = event.target.value;
    return onChange(selectedValue);
  }

  makeOptionElement({ value, text }) {
    return (
      <option key={value} value={value}> 
        {text || value} 
      </option>
    );
  }
}