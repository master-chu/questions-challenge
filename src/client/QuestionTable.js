import React from 'react';
import TableHeader from './TableHeader';
import QuestionRow from './QuestionRow';

export default class QuestionTable extends React.PureComponent {
  headerData = [
    { text: 'ID', sortKey: 'questionId'},
    { text: 'Question', sortKey: 'questionText'},
    { text: 'Answer', sortKey: 'answer'},
    { text: 'Distractors', sortKey: 'distractors'}
  ]

  constructor(props) {
    super(props);
    this.makeTableHeaders = this.makeTableHeaders.bind(this);
    this.makeQuestionRow = this.makeQuestionRow.bind(this);
  }

  render() {
    let { questions, sortBy, orderAsc } = this.props;
    let headers = this.makeTableHeaders();
    let rows = questions.map(this.makeQuestionRow);

    return (
      <table className="table is-small is-fullwidth is-striped is-hoverable">
        <thead>
          <tr>
            {headers}
            <th></th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }

  makeTableHeaders() {
    let {
      sortBy,
      orderAsc,
      onHeaderClick
    } = this.props;

    return this.headerData.map(({text, sortKey}) => {
      return (
        <TableHeader
          key={sortKey}
          text={text}
          sortKey={sortKey}
          sortBy={sortBy}
          orderAsc={orderAsc}
          onClick={onHeaderClick} />
      );
    });
  }

  makeQuestionRow({ questionId, questionText, answer, distractors }) {
    let { deleteQuestion } = this.props;

    return (
      <QuestionRow 
        key={questionId}
        questionId={questionId} 
        questionText={questionText} 
        answer={answer} 
        distractors={distractors} 
        deleteQuestion={deleteQuestion} />
    );
  }
}
