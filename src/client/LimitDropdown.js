import React from 'react';
import Dropdown from './Dropdown';

export default class LimitDropdown extends React.PureComponent {
  render() {
    let { onChange, limit } = this.props;

    const label = "Limit";
    const options = [{
      value: 5
    }, {
      value: 10
    }, {
      value: 25
    }];

    return (
      <Dropdown
        currentValue={limit}
        onChange={onChange}
        label={label}
        options={options} />
    );
  }
}