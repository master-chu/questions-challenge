import React from 'react';

export default class QuestionCreationModal extends React.PureComponent {
  initialState = {
    questionText: '',
    answer: '',
    distractors: ''
  }

  constructor(props) {
    super(props);

    this.state = this.initialState;

    this.close = this.close.bind(this);
    this.save = this.save.bind(this);
    this.change = this.change.bind(this);
  }

  render() {
    let { isVisible } = this.props;
    let {
      questionText,
      answer,
      distractors
    } = this.state;
    let className = `modal${isVisible ? ' is-active' : ''}`;

    return (
      <div className={className}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Create New Question</p>
            <button onClick={this.close} className="delete" aria-label="close"></button>
          </header>
          <section className="modal-card-body">
            <div className="field">
              <label className="label">Question Text</label>
              <div className="control">
                <input onChange={this.change('questionText')} value={questionText} className="input" type="text" placeholder="What is 2 + 2?" />
              </div>
            </div>
            <div className="field">
              <label className="label">Answer</label>
              <div className="control">
                <input onChange={this.change('answer')} value={answer} className="input" type="text" placeholder="4" />
              </div>
            </div>
            <div className="field">
              <label className="label">Distractors</label>
              <div className="control">
                <input onChange={this.change('distractors')} value={distractors} className="input" type="text" placeholder="1, 3, -2" />
              </div>
            </div>
          </section>
          <footer className="modal-card-foot">
            <button disabled={!this.canSave()} onClick={this.save} className="button is-success">Save</button>
            <button onClick={this.close} className="button">Cancel</button>
          </footer>
        </div>
      </div>
    );
  }

  canSave() {
    let { questionText, answer, distractors } = this.state;
    return !!(questionText && answer && distractors);
  }

  change(fieldName) {
    return (event) => {
      let updatedValue = event.target.value;
      let newState = {};
      newState[fieldName] = updatedValue;
      this.setState(newState);
    };
  }

  close() {
    let { onClose } = this.props;
    this.setState(this.initialState);
    onClose();
  }

  save() {
    let { 
      onClose,
      onSave 
    } = this.props;
    let {
      questionText,
      answer,
      distractors
    } = this.state;
    
    onSave({
      questionText,
      answer,
      distractors
    });
    onClose();
  }
}