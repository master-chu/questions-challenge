import React from 'react';
import axios from 'axios';
import LimitDropdown from './LimitDropdown';
import Pagination from './Pagination';
import CreateButton from './CreateButton';
import QuestionTable from './QuestionTable';
import QuestionCreationModal from './QuestionCreationModal';
import Notification from './Notification';

export default class App extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      count: 0,
      questions: [],
      isLoading: false,
      limit: 10,
      isCreationModalVisible: false,
      sortBy: 'questionId',
      orderAsc: true,
      page: 1,
      notification: {}
    };

    this.toggleCreationModal = this.toggleCreationModal.bind(this);
    this.reloadQuestions = this.reloadQuestions.bind(this);
    this.deleteQuestion = this.deleteQuestion.bind(this);
    this.createQuestion = this.createQuestion.bind(this);
    this.setLimit = this.setLimit.bind(this);
    this.sort = this.sort.bind(this);
    this.goToPage = this.goToPage.bind(this);
    this.notify = this.notify.bind(this);
    this.clearNotification = this.clearNotification.bind(this);
  }

  componentDidMount() {
    this.reloadQuestions();
  }

  render() {
    let { 
      count,
      questions,
      isLoading,
      limit,
      isCreationModalVisible,
      sortBy,
      orderAsc,
      page,
      notification
    } = this.state;

    return (
      <div className="container">
        <h2 className="title is-2"> Questions </h2>
        <Notification 
          {...notification} 
          onClickDelete={this.clearNotification} />
        <div className="level">
          <div className="level-left">
            <span className="level-item">
              <LimitDropdown limit={limit} onChange={this.setLimit} />
            </span>
            <span className="level-item">
              <Pagination 
                onSelectPage={this.goToPage}
                currentPage={page}
                numRecords={count}
                pageSize={limit} />
            </span>
          </div>
          <div className="level-right">
            <CreateButton onClick={this.toggleCreationModal} />
          </div>
        </div>
        <QuestionTable
          questions={questions} 
          sortBy={sortBy}
          orderAsc={orderAsc}
          onHeaderClick={this.sort}
          deleteQuestion={this.deleteQuestion} />
        <QuestionCreationModal 
          onSave={this.createQuestion}
          onClose={this.toggleCreationModal}
          isVisible={isCreationModalVisible} />
      </div>
    );
  }

  toggleCreationModal() {
    this.setState((prevState) => { 
      return {
        isCreationModalVisible: !prevState.isCreationModalVisible 
      };
    });
  }

  reloadQuestions() {
    let { 
      limit,
      sortBy,
      orderAsc,
      page
    } = this.state;

    let order = orderAsc ? 'asc' : 'desc';

    return new Promise((resolve, reject) => {
      this.setState({ isLoading: true }, () => {
        axios.get('/api/questions', {
          params: {
            limit,
            sortBy,
            order,
            page
          }
        }).then((response) => {
          let { questions, count } = response.data;
          this.setState({ 
            isLoading: false,
            questions,
            count
          }, () => resolve(questions));
        }).catch((error) => {
          this.notify({ 
            type: 'danger',
            content: `Failed to retrieve questions: ${JSON.stringify(error.response.data)}`
          });
          reject(error);
        });
      });
    });
  }

  deleteQuestion(questionId, questionText) {
    axios.delete(`/api/questions/${questionId}`)
      .then(() => {
        this.reloadQuestions();
        this.notify({
          type: 'success',
          content: `Successfully deleted question: "${questionText}"`
        });
      }).catch((error) => {
        this.notify({ 
          type: 'danger',
          content: `Failed to delete question: ${JSON.stringify(error.response.data)}`
        });
      });
  }

  createQuestion({ questionText, answer, distractors }) {
    axios.post(`/api/questions`, {
      questionText,
      answer,
      distractors
    }).then((response) => {
      let { questionText } = response.data;
      this.reloadQuestions();
      this.notify({
        type: 'success',
        content: `Successfully created a new question: "${questionText}"`
      });
    }).catch((error) => {
      this.notify({ 
        type: 'danger',
        content: `Failed to create question: ${JSON.stringify(error.response.data)}`
      });
    });
  }

  setLimit(limit) {
    this.setState({ limit }, () => {
      this.reloadQuestions();
    });
  }

  sort(sortKey, orderAsc) {
    this.setState((prevState) => {
      return { 
        sortBy: sortKey,
        orderAsc: prevState.sortBy === sortKey ? !orderAsc : true
      };
    }, () => {
      this.reloadQuestions();
    });
  }

  goToPage(page) {
    this.setState({ page }, () => {
      this.reloadQuestions();
    });
  }

  notify({ type, content }) {
    this.setState({
      notification: { type, content }
    });
  }

  clearNotification() {
    this.setState({ notification: {} });
  }
}