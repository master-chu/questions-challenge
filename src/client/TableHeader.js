import React from 'react';

export default class TableHeader extends React.PureComponent {
  render() {
    let { 
      text, 
      sortKey,
      sortBy,
      orderAsc,
      onClick 
    } = this.props;

    let icon = orderAsc ? 'fa-sort-alpha-up' : 'fa-sort-alpha-down';
    let iconClass = sortKey === sortBy ? ` ${icon}` : '';

    return (
      <th>
        <a onClick={() => onClick(sortKey, orderAsc)} target="_blank">
          {`${text} `}
          <span className="icon is-small">
            <i className={`fas${iconClass}`}></i>
          </span>
        </a>
      </th>
    );
  }
}