const sqlite3 = require('sqlite3').verbose();
const readline = require('readline');
const fs = require('fs');
const Promise = require("bluebird");

let db = new sqlite3.Database(':memory:', (error) => error && console.error(error));
const sourceCSV = readline.createInterface({
  input: fs.createReadStream(__dirname + '/../../lib/code_challenge_question_dump.csv')
});

module.exports = {
  get: function(id) {
    return new Promise((resolve, reject) => {
      db.get(`
        SELECT question_id, question_text, answer, distractors
        FROM questions
        WHERE question_id=?;
      `, [ id ], function(error, row) {
        if (error) {
          reject(error);
        } else {
          let question = rowToQuestion(row);
          resolve(question);
        }
      });
    });
  },

  all: function({ sortBy = 'question_id', order = 'DESC', page = 1, limit = 10 }) {
    return new Promise((resolve, reject) => {
      page = parseInt(page, 10);
      limit = parseInt(limit, 10);

      let offset = (page - 1) * limit;

      db.all(`
        SELECT count(1) AS count
        FROM questions;
      `, (error, rows) => {
        let count = rows[0].count;

        if (error) { 
          reject(error);
          return;
        }

        db.all(`
          SELECT question_id, question_text, answer, distractors
          FROM questions
          ORDER BY ${sortBy} ${order}
          LIMIT ?, ?;
        `, [ offset, limit ], function(error, rows) {
          if (error) { 
            reject(error);
          } else {
            let questions = rows.map(rowToQuestion);
            resolve({
              questions,
              count
            });
          }
        });
      });

    });
  },

  create: function(questionText, answer, distractors) {
    return new Promise((resolve, reject) => {
      db.run(`
        INSERT INTO questions (question_text, answer, distractors)
        VALUES (?, ?, ?);
      `, [ questionText, answer, distractors ], function(error) {
        if (error) {
          reject(error);
        } else {
          let questionId = this.lastID;
          resolve({ 
            questionId,
            questionText,
            answer,
            distractors
          });
        }
      });
    });
  },

  update: function(questionId, questionText, answer, distractors) {
    return new Promise((resolve, reject) => {
      db.run(`
        UPDATE questions
        SET question_text=?, answer=?, distractors=?
        WHERE question_id=?;
      `, [ questionText, answer, distractors, questionId ], function(error) {
        if (error) {
          reject(error);
        } else {
          resolve({ 
            questionId,
            questionText,
            answer,
            distractors
          });
        }
      });
    });
  },

  delete: function(questionId) {
    return new Promise((resolve, reject) => {
      db.run(`
        DELETE FROM questions
        WHERE question_id=?;
      `, [ questionId ], function(error) {
        if (error) {
          reject(error);
        } else {
          let isDeleted = this.changes === 1;
          resolve(isDeleted);
        }
      });
    });
  },

  seedQuestionsTable: function() {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(`
          CREATE TABLE questions (
            question_id INTEGER PRIMARY KEY,
            question_text TEXT,
            answer TEXT,
            distractors TEXT
          );
        `, () => console.log('Created `questions` table'));

        let isHeaderRow = true;
        sourceCSV.on('line', (line) => {
          if (!isHeaderRow) {
            let [ question, answer, distractors ] = line.split('|'); 

            db.run(`
              INSERT INTO questions (question_text, answer, distractors)
              VALUES (?, ?, ?);
            `, [ question, answer, distractors ]);
          } else {
            isHeaderRow = false;
          }
        });

        sourceCSV.on('close', () => {
          console.log('Inserted values into DB');
          resolve();
        });
      });
    });
  },
};

function rowToQuestion(row) {
  return {
    questionId: row.question_id,
    questionText: row.question_text,
    answer: row.answer,
    distractors: row.distractors
  };
}