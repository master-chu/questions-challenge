const express = require('express');
const bodyParser = require('body-parser');

const app = express();

module.exports = {
  start: function(db) {
    app.use(bodyParser.json());

    app.get('/api/questions', (req, res) => {
      let { sortBy, order, page, limit } = req.query;

      sortBy = toDbColumn(sortBy);
      order = toSortOrder(order);

      db.all({ sortBy, order, page, limit }).then((questions) => {
        res.json(questions);
      }).catch(sendError(res));
    });

    app.get('/api/questions/:id', (req, res) => {
      let { id } = req.params;
      db.get(id).then((question) => {
        res.json(question);
      }).catch(sendError(res));
    });

    app.post('/api/questions', (req, res) => {
      let { questionText, answer, distractors } = req.body;
      db.create(questionText, answer, distractors).then((question) => {
        res.status(201).json(question);
      }).catch(sendError(res));
    });

    app.put('/api/questions/:id', (req, res) => {
      let { questionText, answer, distractors } = req.body;
      let { id } = req.params;
      db.update(id, questionText, answer, distractors).then((question) => {
        res.status(200).json(question);
      }).catch(sendError(res));
    });

    app.delete('/api/questions/:id', (req, res) => {
      let { id } = req.params;
      db.delete(id).then((isDeleted) => {
        if (isDeleted) {
          res.sendStatus(204);
        } else {
          Promise.reject('No question with question_id ${id} was found', 404);
        }
      }).catch(sendError(res));
    });

    app.use(express.static('dist'));
    app.listen(8080, () => console.log('Listening on port 8080'));
  }
};

function sendError(res) {
  return (error, status = 500) => {
    console.error(error);
    res.status(status).json({ error });
  };
}

function toDbColumn(paramName) {
  const columns = {
    questionId: 'question_id',
    questionText: 'question_text',
    answer: 'answer',
    distractors: 'distractors'
  };
  return columns[paramName];
}

function toSortOrder(order = '') {
  const orders = {
    asc: 'ASC',
    desc: 'DESC'
  };
  return orders[order.toLowerCase()];
}