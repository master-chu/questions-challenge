const db = require('./db');
const server = require('./server');

db.seedQuestionsTable().then(() => {
  server.start(db);
});