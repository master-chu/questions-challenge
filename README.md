# Chris Chu - Questions challenge 2017-08-21 #

## How to run ##
Please use Node version `10.4.0` for best results, since this is what I developed with.

```
npm install
npm start
```
Open `http://localhost:8080` in your browser to view the webpage.

## API ##

#### List All Questions ####
```
GET /api/questions
```
**Query params**
* sortBy - string (`questionId`, `questionText`, `answer`, `distractors`)
* order - string (`asc`, `desc`)
* limit - integer
* page - integer

#### Get Question ####
```
GET /api/questions/:id
```

#### Create Question ####
```
POST /api/questions
```
**Body**
* questionText - string
* answer - string
* distractors - string

#### Edit Question ####
```
PUT /api/questions/:id
```
**Body**
* questionText - string
* answer - string
* distractors - string

#### Delete Question ####
```
DELETE /api/questions/:id
```

## Notes ##

Due to time constraints, the following features were dropped or partially completed:
* I did not have a chance to test with lower versions of Node, so please use `10.4.0` if you can.
* I used an in-memory sqlite database instead of a real one for easier setup and teardown. I recognize that this gives a false sense of real-world database performance, and does not scale beyond a certain point.
* UI edit existing question - The API endpoint should still work, but I did not create a way in the UI to actually edit questions once they are created.
* Filtering - there is no way to search for a question, but adding a `WHERE ... LIKE ...` clause on a debounced endpoint could provide this functionality
* Styled delete modal - currently, the delete functionality just uses `window.confirm` instead of a nice modal.
* Advanced pagination - I used a simple dropdown instead of the full multi-button pagination common on most webpages with paged, tabular data.
* Advanced form validation - Currently there are no checks on form inputs, aside from whether or not they exist (however, values are safely sanitized before DB insertion).
* Better server-side errors - Only a few methods return meaningful errors, everything else forwards the database error if one is found.
* Loading spinner - I built the mechanism to show one, but I did actually render the loading spinner and disable inputs.
* Create/update time columns in the DB - this would allow for easier sorting and auditing
